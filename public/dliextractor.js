var dliExtractor = (function(){

	var pageId='xxxyyyyzzz',selectors = [],pathPrefix = '/dliextractor';

	var getPageId = function(){

		get("/id");
		console.log("getPageId");
	};

	var getSelectors = function(){

		get("/selectors");

		console.log("getSelectors");
	};

	var getNextPage = function(){
		console.log("getNextPage");
	};

	var sendResult = function(){
		console.log("sendResult");
	};

	var sendStatus = function(){
		send("/status/" + pageId, {
			status : 'good'
		});
		console.log("sendStatus");
	};

	var get = function(path){
		$.ajax({
			type: "GET",
			url: pathPrefix + path,
			success: function(a,b){
				console.log('success');
				console.log(a);
				console.log(b);
			},
			dataType: 'json'
		});
	};

	var send = function(path,data){
		$.ajax({
			type: "POST",
			url: pathPrefix + path,
			data: data,
			success: function(a,b){
				console.log('success');
				console.log(a);
				console.log(b);
			},
			dataType: 'json'
		});
	};

	return {
		getPageId : getPageId,
		getSelectors : getSelectors,
		getNextPage : getNextPage,
		sendResult : sendResult,
		sendStatus : sendStatus
	}
})();

//$(function(){
//	dliExtractor.getPageId();
//	dliExtractor.sendStatus();
//});