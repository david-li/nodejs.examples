var http = require('http');
var path = require('path');
var express = require('express');
var app = express();

var server = http.createServer(app);

server.listen(3000, function() {
	console.log("Server listening on port 3000.");
});

server.on('request',function (req, res) {
	//console.log("req.headers");
	//console.log(req.headers);
})

app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  next();
});

app.use('/public', express.static(__dirname + '/public'));

app.use('/dliextractor', function(req, res, next){

  console.log('=============dliextractor============');
  res.header('Content-Type', 'text/json');
  res.send("{ very : 'good'}");
  next();
});


app.use(function(err, req, res, next){
	if(err){
		console.error(err.stack);
		res.send(500, 'Something broke!');
	}else{
		next();
	}
});
