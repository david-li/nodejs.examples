var http = require('http');
var path = require('path');
var express = require('express');
var app = express();

var server = http.createServer(app);
// var socketServer = require('./lib/socket_server');
// socketServer.listen(server);


server.listen(3000, function() {
	console.log("Server listening on port 3000.");
});

app.enable('trust proxy');

app.use(function(req, res, next){
  console.log('%s %s', req.method, req.url);
  console.log(req.headers);
  next();
});

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3001');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// app.use(function(req, res, next) {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Methods', 'GET, OPTIONS');
//   res.header('Access-Control-Allow-Headers', 'Content-Type');
//   next();
// });

app.use('/public', express.static(__dirname + '/public'));

app.use(function(err, req, res, next){
	if(err){
		console.error(err.stack);
		res.send(500, 'Something broke!');
	}else{
		next();
	}
});
