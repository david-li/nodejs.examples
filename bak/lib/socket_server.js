var socketio = require('socket.io');
var io;

exports.listen = function(server) {
	io = socketio.listen(server);

	// io.set('origins','*');
	// io.set('transports', [
	//     'websocket'
	//     , 'flashsocket'
	//     , 'htmlfile'
	//     , 'xhr-polling'
	//     , 'jsonp-polling'
	// ]);
	
	io.set('log level', 3);
	io.set('match origin protocol',true);
	io.set('resource','http://socket.dli.com:3000/socket.io');
	io.sockets.on(
				'connection',
				function (socket) {
					socket.emit('connectionResponse', {
						success: true,
						info: "connected ... ..."
					});
					socket.on('pageUrl', function(content) {
						console.log(socket.id);
						console.log(content);
					});
				}
	);

	// io.of("/testtesttest").on('connection',function(socket){
	// 	console.log('22222222222222222222222')
	// });

	// io.of("/testtesttest").on('connection',function(socket){
	// 	console.log('http://socket.dli.com:3000/socket.io');
	// });
	

	setInterval(tick, 1000);
};

function tick () {
	var now = new Date().toUTCString();
	io.sockets.send(now);
}

