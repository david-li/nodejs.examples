var http = require('http');
var fs = require('fs');
var path = require('path');
var mime = require('mime');
var cache = {};

var server = http.createServer();

server.listen(80, function() {
  console.log("Server listening on port 80.");
});

var socketServer = require('./lib/socket_server');
socketServer.listen(server);
