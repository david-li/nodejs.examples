var selectorGroup = {
	pageId : 10001000,
	selectors : [
		// {
		// 	id : 1000,
		// 	js : function(){ return $("#page_maprice").text() }
		// },
		// {
		// 	id : 1001,
		// 	js : function(){ return $("#jd-price").text() }
		// },
		// {
		// 	id : 1002,
		// 	js : function(){ return $("ul.detail-list li").text() }
		// },
		{
			id : 1003,
			js : function(){ return $("#name h1").text() }
		}
		// ,
		// {
		// 	id : 1004,
		// 	js : function(){ return $("table.Ptable tr").text() }
		// },
		// {
		// 	id : 1005,
		// 	js : function(){ return $("#product-detail ul.tab li .hl_blue").parent().parent().click() },
		// 	sleep : 1000
		// },
		// {
		// 	id : 1006,
		// 	js : function(){ return $("#comments-list .comment-content dl dd").text() }
		// },
		// {
		// 	id : 1007,
		// 	js : function(){ return $("#comments-list .comment-content dl:eq(1) dd").text() }
		// }

	]
};


handler.print(selectorGroup.id);


function processSelectors () {
	while(selectorGroup.selectors.length > 0){
		var currentSelector = selectorGroup.selectors.shift();

		handler.print("selectorId : " + currentSelector.id);
		handler.print(currentSelector.js());

		if(currentSelector.sleep){
			//seep for certain time ,then continue 
			handler.print('sleep : ' + currentSelector.sleep);
			setTimeout(processSelectors, currentSelector.sleep);
			return;
		}
	}

	handler.done('success');

	//load the next URL
	var loadNewUrlTimer = setInterval(function () {
		var newUrl = handler.next();
	    if(newUrl != ''){
	        clearInterval(loadNewUrlTimer);
	        location.href = newUrl;
	    }
	}, 100);

}



processSelectors();