var http = require('http'),
    fs = require('fs'),
    sys = require('sys');

/**
	here should add the parse request url feature.
	return different JS extrac code for different url
*/

fs.readFile('./extractData.js', function (err, js) {
    if (err) {
        throw err; 
    }
    http.createServer(function(request, response) {

    	if(request && request.headers && request.headers.referer){
			console.log(request.headers.referer);
    	}else{
 			console.log('no referer');
    	}
        response.writeHeader(200, {
        							"Content-Type"	: "text/javascript",
        							"Cache-Control"	: "no-cache"
    							});  
        
        response.write(js);  
        response.end();
    }).listen(3000);
});

sys.puts("Server running at http://127.0.0.1:3000");