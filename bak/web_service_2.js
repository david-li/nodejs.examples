var sys   = require('sys'),
    http  = require('http');

/*
/add/2/2         =>4
/sub/103/42      =>61
/mul/3/2         =>6
/div/100/25      =>4
*/

var operations = {
	add : function  (a,b) { return a + b; },
	sub : function  (a,b) { return a - b; },
	mul : function  (a,b) { return a * b; },
	div : function  (a,b) { return a / b; }
};

http.createServer(function(req,res){
	var parts = req.url.split("/"),
	    op = operations[parts[1]],
		a  = parseInt(parts[2], 10),
		b  = parseInt(parts[3], 10);

     sys.puts(sys.inspect(parts));

	var result = op ? op(a,b) : "Error";

	res.writeHead(200,{"Content-Type": 'text/plain'});
	res.end("" + result);
	// res.writeHead(200,{'Content-Type': 'text/html'});
	// res.write("<h1>Hello World!</h1>");
	// res.write("<h2>This is the end!<h2>");
}).listen(3000,"127.0.0.1");

sys.puts("Server running at http://127.0.0.1:3000");