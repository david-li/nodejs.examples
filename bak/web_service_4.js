var http = require('http'),
    fs = require('fs'),
    sys = require('sys');


fs.readFile('./extractData.js', function (err, js) {
    if (err) {
        throw err; 
    }
    http.createServer(function(request, response) {

    	console.log(request.headers.referer);
        response.writeHeader(200, {"Content-Type": "text/javascript"});  
        response.write(js);  
        response.end();
    }).listen(3000);
});

sys.puts("Server running at http://127.0.0.1:3000");